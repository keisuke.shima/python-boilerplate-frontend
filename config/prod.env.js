if (process.env.NODE_ENV === 'production' && !process.env.GOOGLE_ANALYTICS_UA) {
  console.log('Error: process.env.GOOGLE_ANALYTICS_UA not set')
  console.log('Run `source env-secrets.sh before building for production.')
  process.exit(1)
}

module.exports = {
  NODE_ENV: '"production"',
  GOOGLE_ANALYTICS_UA: `"${process.env.GOOGLE_ANALYTICS_UA}"`
}

