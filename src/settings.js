const pjson = require('../package.json')

// Replaced by WebPack
const IS_DEV = process.env.NODE_ENV === 'development'
const IS_TEST = process.env.NODE_ENV === 'testing'
const IS_PRODUCTION = process.env.NODE_ENV === 'production'

let settings = {
  API_BASE_URL: null,
  GOOGLE_ANALYTICS_UA: process.env.GOOGLE_ANALYTICS_UA,  // coming from webpack via `/config/*.env.js`
  ENABLE_API_CACHE: false,

  // Feature flags
  ENABLE_FEATURE_SETTINGS: true,
  VERSION: pjson.version
}

if (IS_DEV) {
  settings.API_BASE_URL = 'http://0.0.0.0:8000'
  // settings.API_BASE_URL = 'https://www.python-boilerplate.com'
} else if (IS_TEST) {
  throw new Error('not yet implemented')
} else if (IS_PRODUCTION) {
  settings.API_BASE_URL = 'https://www.python-boilerplate.com'
  settings.ENABLE_API_CACHE = true
}

// console.log(settings)

export { settings }
export const API_BASE_URL = settings.API_BASE_URL
export const GOOGLE_ANALYTICS_UA = settings.GOOGLE_ANALYTICS_UA
export const ENABLE_API_CACHE = settings.ENABLE_API_CACHE

export const ENABLE_FEATURE_SETTINGS = settings.ENABLE_FEATURE_SETTINGS
export const VERSION = settings.VERSION
