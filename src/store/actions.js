// actions are functions that causes side effects and can involve
// asynchronous operations.

import * as types from './mutation-types'
import * as consts from './constants'
import { API_BASE_URL, ENABLE_API_CACHE, VERSION } from '../settings'

import Vue from 'vue'
import Resource from 'vue-resource'
import { router } from '../router'

Vue.use(Resource)

export const showError = ({ commit, state }, error) => {
  router.push('/error')
  commit(types.SET_ERRORS, [error])
}

const setBoilerplate = (commit, state, responseJson, boilerplateConfig) => {
  commit(types.SET_CURRENT_BOILERPLATE, responseJson)
  commit(types.SET_CURRENT_BOILERPLATE_CONFIG, boilerplateConfig)

  // Make sure there is a valid tab selected
  let filenames = []
  responseJson.files.forEach((f) => filenames.push(f.name))
  if (!state.selectedFilename || filenames.indexOf(state.selectedFilename) === -1) {
    showFile({ commit }, responseJson.files[0].name)
  }
}

export const generateBoilerplate = ({ commit, state, getters }) => {
  const config = getters.getBoilerplateConfig

  if (ENABLE_API_CACHE) {
    const cachedResponse = state.apiResponseCache[config]
    if (cachedResponse) {
      console.log('found a cached response for', config)
      setBoilerplate(commit, state, cachedResponse, config)
      return
    }
  }

  const url = `${API_BASE_URL}/v2/boilerplate/${config}/index.json?v=${VERSION}`
  console.log('requesting', url)

  commit(types.SET_IS_LOADING, true)
  commit(types.SET_ERRORS, [])

  return Vue.http.get(url)
    .then((response) => {
      return response.json()
    })
    .then((responseJson) => {
      // console.log('res json', responseJson)
      commit(types.SET_IS_LOADING, false)
      if (responseJson.error) {
        console.error(responseJson.error)
        showError({ commit, state }, responseJson.error)
        return
      }

      commit(types.STORE_API_RESPONSE, { apiRoute: config, apiResponse: responseJson })
      setBoilerplate(commit, state, responseJson, config)
    })
    .catch((e) => {
      console.error(e)
      showError({ commit, state }, 'Could not connect to backend')
      commit(types.SET_IS_LOADING, false)
    })
}

export const generateBoilerplateByRoute = ({ commit, state, getters }) => {
  const configFromRoute = state.route.params.bpConfig
  const configArray = configFromRoute.split('+')
  // console.log('generateBoilerplateByRoute. old config:', getters.lastBoilerplateConfig, 'new config:', configFromRoute)

  if (configFromRoute === state.currentBoilerplateConfig) {
    // If the config from the route is the same as the current config, do nothing
    return
  }

  // The route contains a new configuration. Update state and generate!
  const pythonVersion = (configArray.indexOf(consts.PY_VERSION_2) !== -1) ? consts.PY_VERSION_2 : consts.PY_VERSION_3
  commit(types.SET_PY_VERSION, pythonVersion)
  commit(types.SET_USE_FLASK, configArray.indexOf(consts.CONF_FLASK) !== -1)
  commit(types.SET_USE_GITIGNORE, configArray.indexOf(consts.CONF_GITIGNORE) !== -1)
  commit(types.SET_EXECUTABLE, configArray.indexOf(consts.CONF_EXECUTABLE) !== -1)
  commit(types.SET_USE_ARGPARSE, configArray.indexOf(consts.CONF_ARGPARSE) !== -1)
  commit(types.SET_USE_LOGGING, configArray.indexOf(consts.CONF_LOGGING) !== -1)
  commit(types.SET_USE_UNITTEST, configArray.indexOf(consts.CONF_UNITTEST) !== -1)
  commit(types.SET_USE_PYTEST, configArray.indexOf(consts.CONF_PYTEST) !== -1)
  commit(types.SET_USE_TOX, configArray.indexOf(consts.CONF_TOX) !== -1)

  generateBoilerplate({ commit, state, getters })
}

export const routeToBoilerplate = (bpConfig) => {
  // console.log('route', bpConfig)
  router.push({ name: 'boilerplate', params: { bpConfig } })
}

export const setPythonVersion2 = ({ commit, getters }) => {
  commit(types.SET_PY_VERSION, consts.PY_VERSION_2)
  routeToBoilerplate(getters.getBoilerplateConfig)
}

export const setPythonVersion3 = ({ commit, getters }) => {
  commit(types.SET_PY_VERSION, consts.PY_VERSION_3)
  routeToBoilerplate(getters.getBoilerplateConfig)
}

export const setFlask = ({ commit, getters }, useFlask) => {
  commit(types.SET_USE_FLASK, useFlask)
  routeToBoilerplate(getters.getBoilerplateConfig)
}

export const setGitignore = ({ commit, getters }, useGitignore) => {
  commit(types.SET_USE_GITIGNORE, useGitignore)
  routeToBoilerplate(getters.getBoilerplateConfig)
}

export const setExecutable = ({ commit, getters }, isExecutable) => {
  commit(types.SET_EXECUTABLE, isExecutable)
  routeToBoilerplate(getters.getBoilerplateConfig)
}

export const setUseArgparse = ({ commit, getters }, useArgparse) => {
  commit(types.SET_USE_ARGPARSE, useArgparse)
  routeToBoilerplate(getters.getBoilerplateConfig)
}

export const setUseLogging = ({ commit, getters }, useLogging) => {
  commit(types.SET_USE_LOGGING, useLogging)
  routeToBoilerplate(getters.getBoilerplateConfig)
}

export const setUseUnittest = ({ commit, getters }, useUnittest) => {
  commit(types.SET_USE_UNITTEST, useUnittest)
  routeToBoilerplate(getters.getBoilerplateConfig)
}

export const setUsePyTest = ({ commit, getters }, usePyTest) => {
  commit(types.SET_USE_PYTEST, usePyTest)
  routeToBoilerplate(getters.getBoilerplateConfig)
}

export const setUseTox = ({ commit, getters }, useTox) => {
  commit(types.SET_USE_TOX, useTox)
  routeToBoilerplate(getters.getBoilerplateConfig)
}

export const showFile = ({ commit }, filename) => {
  commit(types.SET_UI_FILENAME, filename)
}

export const savePreferences = ({ commit, state }) => {
  window.localStorage.setItem('prefs.name', state.preferences.name)
  window.localStorage.setItem('prefs.email', state.preferences.email)
  window.localStorage.setItem('prefs.homepage', state.preferences.homepage)
  window.localStorage.setItem('prefs.projectName', state.preferences.projectName)
}

export const loadPreferences = ({ commit, state }) => {
  commit(types.SET_PREFS_NAME, window.localStorage.getItem('prefs.name'))
  commit(types.SET_PREFS_EMAIL, window.localStorage.getItem('prefs.email'))
  commit(types.SET_PREFS_HOMEPAGE, window.localStorage.getItem('prefs.homepage'))
  commit(types.SET_PREFS_PROJECT_NAME, window.localStorage.getItem('prefs.projectName'))
}
