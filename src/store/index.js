import Vue from 'vue'
import Vuex from 'vuex'
import * as actions from './actions'
import * as types from './mutation-types'
import * as consts from './constants'
import { API_BASE_URL, VERSION } from '../settings'

Vue.use(Vuex)

// root state object.
// each Vuex instance is just a single state tree.
const state = {
  // boilerplate state
  pythonVersion: consts.PY_VERSION_3,
  isExecutable: true,
  useFlask: false,
  useGitignore: false,
  useArgparse: false,
  useLogging: false,
  useTox: false,
  useUnittest: false,
  usePyTest: false,

  // ui state
  isLoading: false,
  selectedFilename: 'main.py',
  currentBoilerplate: null,
  currentBoilerplateConfig: null,
  errors: [],

  // caching of api responses
  apiResponseCache: {},

  // User preferences. Initially try restore from localStorage.
  preferences: {
    name: window.localStorage.getItem('prefs.name'),
    email: window.localStorage.getItem('prefs.email'),
    homepage: window.localStorage.getItem('prefs.homepage'),
    projectName: window.localStorage.getItem('prefs.projectName')
  }
}

// mutations are operations that actually mutates the state.
// each mutation handler gets the entire state tree as the
// first argument, followed by additional payload arguments.
// mutations must be synchronous and can be recorded by plugins
// for debugging purposes.
const mutations = {
  [types.SET_PY_VERSION] (state, pythonVersion) {
    state.pythonVersion = pythonVersion
  },
  [types.SET_USE_FLASK] (state, isUsingFlask) {
    state.useFlask = isUsingFlask
  },
  [types.SET_USE_GITIGNORE] (state, isUsingGitignore) {
    state.useGitignore = isUsingGitignore
  },
  [types.SET_EXECUTABLE] (state, isExecutable) {
    state.isExecutable = isExecutable
  },
  [types.SET_USE_ARGPARSE] (state, useArgparse) {
    state.useArgparse = useArgparse
  },
  [types.SET_USE_LOGGING] (state, useLogging) {
    state.useLogging = useLogging
  },
  [types.SET_USE_UNITTEST] (state, useUnittest) {
    state.useUnittest = useUnittest
    if (useUnittest) {
      state.usePyTest = false
    }
  },
  [types.SET_USE_PYTEST] (state, usePyTest) {
    state.usePyTest = usePyTest
    if (usePyTest) {
      state.useUnittest = false
    }
  },
  [types.SET_USE_TOX] (state, useTox) {
    state.useTox = useTox
  },

  [types.SET_UI_FILENAME] (state, filename) {
    state.selectedFilename = filename
  },
  [types.SET_CURRENT_BOILERPLATE] (state, currentBoilerplate) {
    state.currentBoilerplate = currentBoilerplate
  },
  [types.SET_CURRENT_BOILERPLATE_CONFIG] (state, currentBoilerplateConfig) {
    state.currentBoilerplateConfig = currentBoilerplateConfig
  },
  [types.SET_IS_LOADING] (state, isLoading) {
    state.isLoading = isLoading
  },
  [types.SET_ERRORS] (state, errors) {
    state.errors = errors
  },
  [types.STORE_API_RESPONSE] (state, { apiRoute, apiResponse }) {
    state.apiResponseCache[apiRoute] = apiResponse
  },

  [types.SET_PREFS_NAME] (state, name) {
    state.preferences.name = name
  },
  [types.SET_PREFS_EMAIL] (state, email) {
    state.preferences.email = email
  },
  [types.SET_PREFS_HOMEPAGE] (state, homepage) {
    state.preferences.homepage = homepage
  },
  [types.SET_PREFS_PROJECT_NAME] (state, projectName) {
    state.preferences.projectName = projectName
  }
}

// getters are functions
const getters = {
  isPythonVersion2: state => state.pythonVersion === consts.PY_VERSION_2,
  isPythonVersion3: state => state.pythonVersion === consts.PY_VERSION_3,
  isExecutable: state => state.isExecutable,
  isUsingFlask: state => state.useFlask,
  isUsingGitignore: state => state.useGitignore,
  isUsingArgparse: state => state.useArgparse,
  isUsingLogging: state => state.useLogging,
  isUsingUnittest: state => state.useUnittest,
  isUsingPyTest: state => state.usePyTest,
  isUsingTox: state => state.useTox,

  selectedFilename: state => state.selectedFilename,
  boilerplateFiles: state => state.currentBoilerplate ? state.currentBoilerplate.files : [],
  lastBoilerplateConfig: state => state.currentBoilerplateConfig,
  hasBoilerplate: state => state.currentBoilerplate !== null,
  isLoading: state => state.isLoading,
  currentRoutePath: state => state.route.fullPath,
  errors: state => state.errors,

  getBoilerplateConfig: state => {
    let config = [state.pythonVersion]
    if (state.useFlask) config.push(consts.CONF_FLASK)
    if (state.isExecutable) config.push(consts.CONF_EXECUTABLE)
    if (state.useGitignore) config.push(consts.CONF_GITIGNORE)
    if (state.useArgparse) config.push(consts.CONF_ARGPARSE)
    if (state.useLogging) config.push(consts.CONF_LOGGING)
    if (state.useUnittest) config.push(consts.CONF_UNITTEST)
    if (state.usePyTest) config.push(consts.CONF_PYTEST)
    if (state.useTox) config.push(consts.CONF_TOX)
    return config.join('+')
  },

  getZipUrl: state => {
    return `${API_BASE_URL}/v2/boilerplate/${getters.getBoilerplateConfig(state)}/boilerplate.zip?v=${VERSION}`
  },

  shareWithEmailLink: (state, getters) => {
    return `mailto:?subject=${encodeURIComponent(getters.getTitle)}&body=${encodeURIComponent(window.location.href)}`
  },
  shareWithTwitterLink: (state, getters) => {
    return `https://twitter.com/intent/tweet?text=${encodeURIComponent(getters.getTitle)}: ${encodeURIComponent(window.location.href)}`
  },

  getTitle: state => {
    let title = ''

    // Default title prefix
    if (state.pythonVersion === consts.PY_VERSION_2) {
      title += 'Python 2 '
    } else {
      title += 'Python 3 '
    }

    if (state.useFlask) title += 'Flask '
    title += 'boilerplate'

    let moreItems = []
    // if (state.isExecutable) moreItems.push('executable')
    if (state.useArgparse) moreItems.push('argparse')
    if (state.useGitignore) moreItems.push('gitignore')
    if (state.useLogging) moreItems.push('logging')
    if (state.useUnittest) moreItems.push('unittest')
    if (state.usePyTest) moreItems.push('py.test')
    if (state.useTox) moreItems.push('tox')

    if (moreItems.length) title += ' with ' + moreItems.join(' + ')

    return title
  }
}

// A Vuex instance is created by combining the state, mutations, actions,
// and getters.
export default new Vuex.Store({
  state,
  getters,
  actions,
  mutations
})
