import VueRouter from 'vue-router'

import Boilerplates from './routes/Boilerplates'
import About from './routes/About'
import InfoFlask from './routes/InfoFlask'
import Settings from './routes/Settings'
import NotFound from './routes/NotFound'

export const routes = [
  { path: '/', redirect: '/py3+executable' },
  { path: '/settings', component: Settings },
  { path: '/flask', component: InfoFlask },
  { name: 'error', path: '/error', component: Boilerplates },
  { path: '/about', component: About },
  { name: 'boilerplate', path: '/:bpConfig', component: Boilerplates },
  { path: '*', component: NotFound }
]

// Setup router
export const router = new VueRouter({
  mode: 'history',
  routes
})
