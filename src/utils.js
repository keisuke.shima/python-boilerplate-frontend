import { API_BASE_URL } from './settings'

export const setHtmlTags = (title, description) => {
  document.title = title

  if (description) {
    document.querySelector('meta[name="description"]').content = description
  }

  // The rest is modification of the OpenGraph tags. This is
  // only used to have proper tags for the prerendered pages.

  const location = API_BASE_URL + window.location.pathname + '/'
  // console.log('location:', location)

  // OG Tags
  document.querySelector('meta[property="og:title"]').content = title
  document.querySelector('meta[property="og:description"]').content = description || title
  document.querySelector('meta[property="og:url"]').content = location

  // Twitter description
  document.querySelector('meta[name="twitter:description"]').content = description || title
  document.querySelector('meta[name="twitter:url"]').content = location
}
